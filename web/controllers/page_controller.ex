defmodule RemindVideo.PageController do
  use RemindVideo.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
